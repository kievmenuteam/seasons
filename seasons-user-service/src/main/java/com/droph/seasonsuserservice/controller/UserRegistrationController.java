package com.droph.seasonsuserservice.controller;

import com.droph.seasonsuserservice.common.AccountType;
import com.droph.seasonsuserservice.exception.InvalidCredentialException;
import com.droph.seasonsuserservice.exception.PasswordPolicyException;
import com.droph.seasonsuserservice.rest.UserRequest;
import com.droph.seasonsuserservice.rest.UserResponse;
import com.droph.seasonsuserservice.service.UserServiceFacade;
import com.droph.seasonsuserservice.service.login.UserLoginServiceFacade;
import com.droph.seasonsuserservice.service.validation.UserValidatorFacade;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("user-service")
@Slf4j
public class UserRegistrationController {

    @Autowired
    private UserServiceFacade      userService;
    @Autowired
    private UserValidatorFacade    userRequestValidator;
    @Autowired
    private UserLoginServiceFacade userLoginServiceFacade;

    @GetMapping("/hello")
    public String sayHello() {
        return "Hello, user!";
    }

    @GetMapping("/list/all")
    public List<UserResponse> getAllUsers() {
        return userService.listUsers();
    }

    @GetMapping("/list/login-users")
    public List<UserResponse> getLoggedInUsers() {
        log.info("Retrieving all users");
        return userService.listLoggedInUsers();
    }

    @PostMapping("/register")
    public void registerUser(@RequestBody UserRequest userRequest) {
        try {
            log.info("Trying to register user for {}", userRequest);
            userRequestValidator.validate(userRequest);
            userLoginServiceFacade.addUser(userRequest);
            log.info("User was registered with credentials {}", userRequest);
        } catch (Exception e) {
            log.error("Registration failed {}", userRequest, e);
            throw e;
        }
    }

    @PutMapping("/login")
    public UserResponse loginUser(@RequestParam String email,
                                  @RequestParam(defaultValue = "") String pass,
                                  @RequestParam(required = false) String accountType) {
        UserResponse userResponse;
        try {
            log.info("Trying to login user for email {}, pass {}, accType {}", email, pass, accountType);
            userResponse = userLoginServiceFacade.login(email, pass, AccountType.of(accountType));
            log.info("User logged in {}", userResponse);
        } catch (Exception e) {
            log.error("Login failed for userResponse {}, {}, password {}", email, accountType, pass, e);
            throw e;
        }

        return userResponse;
    }

    @PutMapping("/reLogin")
    public Optional<UserResponse> reLoginUser(@RequestParam String token) {
        Optional<UserResponse> userResponse;
        try {
            log.info("Trying to reLogin user for token {}", token);

            userResponse = userLoginServiceFacade.reLogin(token);

            log.info("reLogin response {} for for token {}", userResponse, token);
        } catch (Exception e) {
            log.error("Login failed for token {}", token, e);
            throw e;
        }
        return userResponse;
    }

    @DeleteMapping("/logout")
    public void logoutUser(@RequestParam String token) {
        log.info("Trying to logout user for token {}", token);

        userService.logout(token);
    }

    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler({PasswordPolicyException.class})
    @ResponseBody
    public String handleNotFound(PasswordPolicyException e) {
        return e.getMessage();
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(InvalidCredentialException.class)
    @ResponseBody
    public String handleInvalidCredentials(InvalidCredentialException e) {
        return e.getMessage();
    }
}