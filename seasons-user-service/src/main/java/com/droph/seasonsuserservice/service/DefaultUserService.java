package com.droph.seasonsuserservice.service;

import com.droph.seasonsuserservice.converters.UserEntityUserResponseConverter;
import com.droph.seasonsuserservice.converters.UserRequestUserEntityConverter;
import com.droph.seasonsuserservice.exception.InvalidCredentialException;
import com.droph.seasonsuserservice.exception.PasswordPolicyException;
import com.droph.seasonsuserservice.persistance.UserEntity;
import com.droph.seasonsuserservice.persistance.UserRepository;
import com.droph.seasonsuserservice.rest.UserRequest;
import com.droph.seasonsuserservice.rest.UserResponse;
import com.droph.seasonsuserservice.service.login.UserLoginManager;
import com.droph.seasonsuserservice.service.login.UserLoginService;
import com.droph.seasonsuserservice.service.validation.UserValidator;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.Clock;
import java.time.Instant;
import java.util.Optional;
import java.util.regex.Pattern;

@AllArgsConstructor
@Slf4j
public class DefaultUserService implements UserLoginService, UserValidator {
    private static final Pattern                         VALID_PASSWORD_PATTERN = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[!@#_%$?]).+$");
    private final        UserRepository                  userRepository;
    private final        UserLoginManager                userLoginManager;
    private final        UserEntityUserResponseConverter userEntityUserResponseConverter;
    private final        UserRequestUserEntityConverter  userRequestUserEntityConverter;
    private final        Clock                           clock;

    @Override
    public boolean validate(UserRequest userRequest) {
        if (isNotValidPasswordPattern(userRequest)) {
            log.error("Invalid password pattern: {}", userRequest.getPassword());
            throw new PasswordPolicyException(
                    "Password should contain upper and lowercase letters, at least one digit and at least one of these: !@#_%$?");
        }
        return true;
    }

    @Override
    public UserResponse login(String email, String password) {
        UserEntity userEntity = userRepository.findByEmailAndPassword(email, password);
        if (userEntity == null) {
            throw new InvalidCredentialException(String.format("Login failed! Invalid password or user %s does not exist.", email));
        }

        Long userId = userEntity.getId();

        String userToken = userLoginManager.getUserToken(userId);
        if (userToken == null) {
            userToken = userLoginManager.logInUser(userEntity);

            UserEntity updateUserEntity = userEntity.toBuilder()
                                                    .lastLoginTs(Instant.now(clock))
                                                    .build();

            userEntity = userRepository.save(updateUserEntity);
        }
        return convertWithToken(userToken, userEntity);
    }

    private UserResponse convertWithToken(String userToken, UserEntity userEntity) {
        return userEntityUserResponseConverter.convert(userEntity)
                                              .toBuilder()
                                              .token(userToken)
                                              .build();
    }

    @Override
    public void addUser(UserRequest userRequest) {
        userRepository.save(userRequestUserEntityConverter.convert(userRequest));
    }

    @Override
    public Optional<UserResponse> reLogin(String token) {
        return userLoginManager.getUserByToken(token)
                               .map(entity -> convertWithToken(token, entity));
    }

    private boolean isNotValidPasswordPattern(UserRequest userRequest) {
        return !VALID_PASSWORD_PATTERN.matcher(userRequest.getPassword()).matches();
    }
}
