package com.droph.seasonsui.service;

import com.droph.seasonsui.model.UserLoginData;
import com.droph.seasonsui.model.UserLoginResponse;
import com.droph.seasonsui.service.external.UserServiceClientFacade;
import com.droph.seasonsuserservice.rest.UserResponse;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
public class UserLoginHandler {
    private final UserServiceClientFacade userServiceClientFacade;

    public Optional<UserLoginResponse> loginUser(UserLoginData userLogin) {
        return userServiceClientFacade.loginUser(userLogin)
                                      .map(this::buildUserLoginResponse);
    }

    public Optional<UserLoginResponse> loginUser(String token) {
        return userServiceClientFacade.loginUser(token)
                                      .map(this::buildUserLoginResponse);
    }

    private UserLoginResponse buildUserLoginResponse(UserResponse ur) {
        return UserLoginResponse.builder()
                                .id(ur.getId())
                                .token(ur.getToken())
                                .name(ur.getName())
                                .build();
    }

    public void logoutUser(String token) {
        userServiceClientFacade.logoutUser(token);
    }
}
