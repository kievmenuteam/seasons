package com.droph.seasonsqueue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeasonsQueueApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeasonsQueueApplication.class, args);
	}
}
