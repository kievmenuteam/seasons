package com.droph.seasonsuserservice;

import com.droph.seasonsuserservice.common.AccountType;
import com.droph.seasonsuserservice.converters.UserEntityUserResponseConverter;
import com.droph.seasonsuserservice.converters.UserRequestUserEntityConverter;
import com.droph.seasonsuserservice.persistance.UserEntity;
import com.droph.seasonsuserservice.persistance.UserRepository;
import com.droph.seasonsuserservice.rest.UserRequest;
import com.droph.seasonsuserservice.rest.UserResponse;
import com.droph.seasonsuserservice.service.login.UserLoginManager;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class SeasonsUserServiceApplicationTests {
    public static final String USER_NAME = "Pasha";
    public static final String VALID_PASSWORD = "123Aa!";
    public static final String USER_EMAIL = "1@1.com";
    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserLoginManager userLoginManager;
    @Autowired
    private UserEntityUserResponseConverter userEntityUserResponseConverter;
    @Autowired
    private UserRequestUserEntityConverter userRequestUserEntityConverter;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @After
    public void tearDown() {
        userLoginManager.getAllLoggedInUsers()
                        .stream()
                        .map(UserEntity::getId)
                        .map(userLoginManager::getUserToken)
                        .forEach(userLoginManager::logOutUser);
        userRepository.deleteAll();
    }

    @Test
    public void shouldReturnEmptyUserList() throws Exception {
        String stringResponse = performSuccessfulRequestAndGetResponseAsString(get("/user-service/list/all"));

        List<UserResponse> actualResponse = objectMapper.readValue(stringResponse, new TypeReference<List<UserResponse>>() {
        });

        assertThat(actualResponse)
                .isEmpty();
    }

    @Test
    public void shouldRegisterDefaultUser() throws Exception {
        UserRequest user = UserRequest.builder()
                                      .name(USER_NAME)
                                      .email(USER_EMAIL)
                                      .password(VALID_PASSWORD)
                                      .accountType(AccountType.DEFAULT)
                                      .build();

        performSuccessfulRegisterUser(user);
        UserResponse expectedResponse = userEntityUserResponseConverter.convert(userRepository.findByEmail(user.getEmail()));

        String stringResponse = performSuccessfulRequestAndGetResponseAsString(get("/user-service/list/all"));
        List<UserResponse> listResponse = objectMapper.readValue(stringResponse, new TypeReference<List<UserResponse>>() {
        });

        assertThat(listResponse)
                .containsOnly(expectedResponse);
    }

    @Test
    public void shouldLoginDefaultUser() {
        UserRequest user = UserRequest.builder()
                                      .name(USER_NAME)
                                      .email(USER_EMAIL)
                                      .password(VALID_PASSWORD)
                                      .accountType(AccountType.DEFAULT)
                                      .build();

        performSuccessfulRegisterUser(user);

        UserResponse response = performSuccessfulLoginRequest(user.getEmail(), user.getPassword());
        UserResponse expectedResponse = userEntityUserResponseConverter.convert(userRepository.findByEmail(user.getEmail()))
                                                                       .toBuilder()
                                                                       .token(response.getToken())
                                                                       .build();

        assertThat(response)
                .isEqualTo(expectedResponse);
    }

    @Test
    public void shouldReturnLoggedInUserFromLoggedInUserList() throws Exception {
        UserRequest user = UserRequest.builder()
                                      .name(USER_NAME)
                                      .email(USER_EMAIL)
                                      .password(VALID_PASSWORD)
                                      .accountType(AccountType.DEFAULT)
                                      .build();

        performSuccessfulRegisterUser(user);

        performSuccessfulLoginRequest(user.getEmail(), user.getPassword());

        String response = performSuccessfulRequestAndGetResponseAsString(get("/user-service/list/login-users"));

        List<UserResponse> actual = objectMapper.readValue(response, new TypeReference<List<UserResponse>>() {
        });

        UserResponse expectedResponse = userEntityUserResponseConverter.convert(userRepository.findByEmail(user.getEmail()));
        assertThat(actual)
                .containsOnly(expectedResponse);
    }

    @Test
    public void shouldFailRegisterUserWith422Status() {
        UserRequest user = UserRequest.builder()
                                      .name(USER_NAME)
                                      .email(USER_EMAIL)
                                      .password("Ololo!")
                                      .accountType(AccountType.DEFAULT)
                                      .build();

        performRegisterUserAndExpectStatus(user, status().isUnprocessableEntity());
    }

    @Test
    public void shouldFailLoginUserWith401Status() throws Exception {
        UserRequest user = UserRequest.builder()
                                      .name(USER_NAME)
                                      .email(USER_EMAIL)
                                      .password(VALID_PASSWORD)
                                      .accountType(AccountType.DEFAULT)
                                      .build();

        performSuccessfulRegisterUser(user);

        performLoginRequestAndExpectStatus(user.getEmail(), user.getPassword() + "1", status().isUnauthorized());
    }

    @Test
    public void shouldLogOutUser() {
        UserResponse userResponse = performSuccessfulRegisterAndLogin();
        performRequest(delete("/user-service/logout")
                               .param("token", userResponse.getToken()), status().isOk());
        assertThat(userLoginManager.getAllLoggedInUsers()).isEmpty();
    }

    @Test
    public void shouldReloginActiveUser() throws IOException {
        UserResponse userResponse = performSuccessfulRegisterAndLogin();

        String response = performReLoginRequest(userResponse.getToken());
        Optional<UserResponse> optionalUserResponse = objectMapper.readValue(response, new TypeReference<Optional<UserResponse>>() {
        });

        assertThat(optionalUserResponse)
                .isNotEmpty()
                .contains(userResponse);
    }

    @Test
    public void shouldReturnEmptyOptionalWhenReloginInactiveUser() throws IOException {
        UserResponse userResponse = performSuccessfulRegisterAndLogin();
        performRequest(delete("/user-service/logout")
                               .param("token", userResponse.getToken()), status().isOk());

        String response = performReLoginRequest(userResponse.getToken());
        Optional<UserResponse> optionalUserResponse = objectMapper.readValue(response, new TypeReference<Optional<UserResponse>>() {
        });

        assertThat(optionalUserResponse)
                .isEmpty();
    }

    @SneakyThrows
    private UserResponse performSuccessfulLoginRequest(String email, String pass) {
        String userLoginResponseJson = performSuccessfulRequestAndGetResponseAsString(put("/user-service/login")
                                                                                              .param("email", email)
                                                                                              .param("pass", pass));
        return objectMapper.readValue(userLoginResponseJson, UserResponse.class);
    }

    @SneakyThrows
    private String performLoginRequestAndExpectStatus(String email, String pass, ResultMatcher statusResultMatcher) {
        return performRequest(put("/user-service/login")
                                      .param("email", email)
                                      .param("pass", pass), statusResultMatcher).andReturn()
                                                                                .getResponse()
                                                                                .getContentAsString();
    }

    @SneakyThrows
    private void performSuccessfulRegisterUser(UserRequest userRequest) {
        String json = objectMapper.writeValueAsString(userRequest);
        performSuccessfulRequestAndGetResponseAsString(post("/user-service/register")
                                                               .contentType(MediaType.APPLICATION_JSON_UTF8)
                                                               .content(json));
    }

    @SneakyThrows
    private void performRegisterUserAndExpectStatus(UserRequest userRequest, ResultMatcher statusResultMatcher) {
        String json = objectMapper.writeValueAsString(userRequest);
        performRequest(post("/user-service/register")
                               .contentType(MediaType.APPLICATION_JSON_UTF8)
                               .content(json), statusResultMatcher);
    }

    @SneakyThrows
    private String performReLoginRequest(String token) {
        return performRequest(put("/user-service/reLogin")
                                      .param("token", token), status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
    }

    @SneakyThrows
    private String performSuccessfulRequestAndGetResponseAsString(MockHttpServletRequestBuilder requestBuilder) {
        return performRequest(requestBuilder, status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
    }

    @SneakyThrows
    private ResultActions performRequest(MockHttpServletRequestBuilder requestBuilder,
                                         ResultMatcher statusResult) {
        return mockMvc.perform(requestBuilder)
                      .andExpect(statusResult);
    }

    private UserResponse performSuccessfulRegisterAndLogin() {
        UserRequest user = UserRequest.builder()
                                      .name(USER_NAME)
                                      .email(USER_EMAIL)
                                      .password(VALID_PASSWORD)
                                      .accountType(AccountType.DEFAULT)
                                      .build();

        performSuccessfulRegisterUser(user);

        return performSuccessfulLoginRequest(user.getEmail(), user.getPassword());
    }

    private void performSuccessfulRegisterLoginAndLogout() {
        UserResponse userResponse = performSuccessfulRegisterAndLogin();

        performRequest(delete("/user-service/logout")
                               .param("token", userResponse.getToken()), status().isOk());
    }
}