package com.droph.seasonsuserservice.exception;

public class PasswordPolicyException extends RuntimeException {
  public PasswordPolicyException(String msg) {
    super(msg);
  }
}
