package com.droph.seasonsui.service;

import com.vaadin.flow.server.VaadinService;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.Cookie;
import java.time.Duration;
import java.util.Arrays;
import java.util.Optional;

@Slf4j
public class TokenService {
    private static final String TOKEN_NAME = "userToken";
    private static final int ONE_DAY = (int) Duration.ofDays(1).getSeconds();

    public Optional<String> getUserToken() {
        return Arrays.stream(VaadinService.getCurrentRequest().getCookies())
                     .filter(c -> TOKEN_NAME.equals(c.getName()))
                     .map(Cookie::getValue)
                     .findAny();
    }

    public void saveUserToken(String token) {
        Cookie cookie = new Cookie(TOKEN_NAME, token);
        cookie.setMaxAge(ONE_DAY);
        cookie.setPath(VaadinService.getCurrentRequest().getContextPath());
        VaadinService.getCurrentResponse().addCookie(cookie);
    }
}
