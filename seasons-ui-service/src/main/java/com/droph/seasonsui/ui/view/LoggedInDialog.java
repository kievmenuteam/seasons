package com.droph.seasonsui.ui.view;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

public class LoggedInDialog extends Dialog {
    public LoggedInDialog() {
        add(new Label("You've been successfully logged in"));
        Button button = new Button("OK");
        button.getElement().getStyle().set("width", "150px");
        button.getElement().getThemeList().add("success primary");
        button.addClickListener(e -> {
            close();
            getUI().ifPresent(ui -> ui.navigate(MenuView.class));
        });
        add(new Html("<br/>"));
        add(button);
        setSizeFull();
    }
}
