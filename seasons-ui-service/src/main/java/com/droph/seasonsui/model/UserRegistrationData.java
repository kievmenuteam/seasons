package com.droph.seasonsui.model;

import lombok.Data;

@Data
public class UserRegistrationData {
    private String name;
    private String email;
    private String password;
}
