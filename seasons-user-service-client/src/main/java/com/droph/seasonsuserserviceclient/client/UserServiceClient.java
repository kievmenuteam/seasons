package com.droph.seasonsuserserviceclient.client;

import com.droph.seasonsuserservice.rest.UserRequest;
import com.droph.seasonsuserservice.rest.UserResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@FeignClient(name = "seasons-user-service", path = "user-service")
public interface UserServiceClient {

    @GetMapping(value = "/hello")
    String sayHello();

    @GetMapping(value = "/list/all")
    List<UserResponse> getAllUsers();

    @GetMapping(value = "/list/login-users")
    List<UserResponse> getLoggedInUsers();

    @PutMapping(value = "/login")
    UserResponse loginUser(@RequestParam("email") String email,
                           @RequestParam("pass") String pass,
                           @RequestParam("accountType") String accountType);

    @PostMapping(value = "/register")
    void registerUser(@RequestBody UserRequest userRequest);

    @PutMapping(value = "/reLogin")
    Optional<UserResponse> reLoginUser(@RequestParam String token);

    @DeleteMapping("/logout")
    void logoutUser(@RequestParam String token);
}
