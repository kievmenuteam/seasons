package com.droph.seasonsuserservice.rest;

import com.droph.seasonsuserservice.common.AccountType;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class UserRequest {
    String name;
    String email;
    String password;
    AccountType accountType;
}
