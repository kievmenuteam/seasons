package com.droph.seasonsuserservice.exception;

public class UserNotFoundException extends RuntimeException {
  public UserNotFoundException(String msg)  {
    super(msg);
  }
}
