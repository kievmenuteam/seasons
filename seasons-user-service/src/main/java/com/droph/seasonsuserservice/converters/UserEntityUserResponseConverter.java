package com.droph.seasonsuserservice.converters;

import com.droph.seasonsuserservice.persistance.UserEntity;
import com.droph.seasonsuserservice.rest.UserResponse;
import org.springframework.core.convert.converter.Converter;

public class UserEntityUserResponseConverter implements Converter<UserEntity, UserResponse> {
    @Override
    public UserResponse convert(UserEntity userEntity) {
        return UserResponse.builder()
                           .id(userEntity.getId())
                           .email(userEntity.getEmail())
                           .name(userEntity.getName())
                           .registerDate(userEntity.getRegistrationTs())
                           .lastLoginDate(userEntity.getLastLoginTs())
                           .build();
    }
}
