package com.droph.seasonsui.ui.view;

import com.droph.seasonsui.service.TokenService;
import com.droph.seasonsui.service.UserGameSessionHandler;
import com.droph.seasonsui.service.UserLoginHandler;
import com.droph.seasonsui.ui.component.LogoutComponent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import lombok.extern.slf4j.Slf4j;

@Route("menu")
@PageTitle("Menu")
@Slf4j
public class MenuView extends HorizontalLayout {
    private final TokenService     tokenService;
    private final UserLoginHandler userLoginHandler;
    private final UserGameSessionHandler userGameSessionHandler;

    public MenuView(TokenService tokenService,
                    UserLoginHandler userLoginHandler, UserGameSessionHandler userGameSessionHandler) {
        this.tokenService = tokenService;
        this.userLoginHandler = userLoginHandler;
        this.userGameSessionHandler = userGameSessionHandler;

        buildUI();
    }

    private void buildUI() {
        Component logoutComponent = new LogoutComponent(tokenService, userLoginHandler, "Hello fellow gamer, main menu under construction");
        add(logoutComponent);
        add(new Html("<br/>"));

        Button playGameButton = new Button("Play game!!!");
        playGameButton.getElement().getStyle().set("width", "400px");
        playGameButton.getElement().getStyle().set("height", "40px");
        playGameButton.getElement().getThemeList().add("success primary");
        playGameButton.setIcon(VaadinIcon.PLAY.create());
        playGameButton.addClickListener(event -> {
            tokenService.getUserToken().ifPresent(userGameSessionHandler::startGame);
            UI.getCurrent().navigate(GameView.class);
        });

        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.add(playGameButton);

        add(buttonsLayout);
    }

}
