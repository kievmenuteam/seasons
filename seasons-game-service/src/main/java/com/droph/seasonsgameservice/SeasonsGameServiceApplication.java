package com.droph.seasonsgameservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeasonsGameServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeasonsGameServiceApplication.class, args);
	}
}
