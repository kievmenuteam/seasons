package com.droph.seasonsuserservice.persistance;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
    UserEntity findByName(String name);

    UserEntity findByEmailAndPassword(String email, String password);

    UserEntity findByEmail(String email);
}
