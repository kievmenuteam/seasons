package com.droph.seasonsuserservice.common;

import java.util.Map;

import static java.util.Arrays.stream;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

public enum AccountType {
    DEFAULT,
    FACEBOOK;

    static final Map<String, AccountType> TYPE_BY_NAME = stream(values()).collect(toMap(Enum::name, identity()));

    public static AccountType of(String name) {
        return TYPE_BY_NAME.getOrDefault(name, DEFAULT);
    }
}
