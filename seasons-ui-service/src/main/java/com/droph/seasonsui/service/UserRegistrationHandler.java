package com.droph.seasonsui.service;

import com.droph.seasonsui.model.UserLoginData;
import com.droph.seasonsui.model.UserLoginResponse;
import com.droph.seasonsui.model.UserRegistrationData;
import com.droph.seasonsui.service.external.UserServiceClientFacade;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UserRegistrationHandler {
    private final UserServiceClientFacade userServiceClientFacade;
    private final UserLoginHandler        userLoginHandler;
    private final TokenService            tokenService;

    public void registerUser(UserRegistrationData userRegistrationData) {
        userServiceClientFacade.registerUser(userRegistrationData);
        userLoginHandler.loginUser(new UserLoginData(userRegistrationData.getEmail(), userRegistrationData.getPassword()))
                        .map(UserLoginResponse::getToken)
                        .ifPresent(tokenService::saveUserToken);
    }
}
