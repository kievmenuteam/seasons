package com.droph.seasonsui.ui.view;

import com.droph.seasonsui.model.UserLoginData;
import com.droph.seasonsui.model.UserLoginResponse;
import com.droph.seasonsui.service.TokenService;
import com.droph.seasonsui.service.UserLoginHandler;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.Binder.Binding;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.util.Optional;


@Route("")
@PageTitle("Login")
@Slf4j
public class LoginView extends FlexLayout {
    private final UserLoginHandler userLoginHandler;
    private final TokenService tokenService;
    private Binder<UserLoginData> userLoginBinder = new Binder<>();
    private TextField loginField;

    public LoginView(UserLoginHandler userLoginHandler,
                     TokenService tokenService) {
        this.userLoginHandler = userLoginHandler;
        this.tokenService = tokenService;

        buildUI();
        loginField.focus();
    }

    private void buildUI() {
        setSizeFull();

        Component loginForm = buildLoginForm();

        FlexLayout centeringLayout = new FlexLayout();
        centeringLayout.setSizeFull();
        centeringLayout.setJustifyContentMode(JustifyContentMode.CENTER);
        centeringLayout.setAlignItems(Alignment.CENTER);
        centeringLayout.add(loginForm);

        Component loginInformation = buildLoginInformation();

        add(loginInformation);
        add(centeringLayout);
    }


    private Component buildLoginForm() {
        FormLayout loginFormLayout = new FormLayout();

        loginField = new TextField();
        Binding<UserLoginData, String> loginBinder = userLoginBinder.forField(loginField)
                                                                    .withValidator(login -> !loginField.getValue()
                                                                                                       .trim()
                                                                                                       .isEmpty(), "Login can not be empty")
                                                                    .bind(UserLoginData::getLogin, UserLoginData::setLogin);
        loginField.addValueChangeListener(event -> loginBinder.validate());
        loginField.setRequiredIndicatorVisible(true);

        PasswordField passwordField = new PasswordField();
        Binding<UserLoginData, String> passwordBinder = userLoginBinder.forField(passwordField)
                                                                       .withValidator(pass -> !passwordField.getValue()
                                                                                                            .trim()
                                                                                                            .isEmpty(), "Password can not be empty")
                                                                       .bind(UserLoginData::getPassword, UserLoginData::setPassword);
        passwordField.addValueChangeListener(event -> passwordBinder.validate());
        passwordField.setRequiredIndicatorVisible(true);

        loginFormLayout.addFormItem(loginField, "Login");
        loginFormLayout.add(new Html("<br/>"));
        loginFormLayout.addFormItem(passwordField, "Password");
        loginFormLayout.add(new Html("<br/>"));

        loginFormLayout.add(buildButtonsLayout());

        loginFormLayout.getElement()
                       .addEventListener("keypress", event -> loginUser())
                       .setFilter("event.key == 'Enter'");

        return loginFormLayout;
    }

    private HorizontalLayout buildButtonsLayout() {
        Button registerButton = new Button("Register account");
        registerButton.getElement().getStyle().set("width", "150px");
        registerButton.addClickListener(buttonClickEvent -> registerButton.getUI()
                                                                          .ifPresent(ui -> ui.navigate(RegistrationView.class)));

        Button loginButton = new Button("Login");
        loginButton.getElement().getStyle().set("width", "150px");
        loginButton.getElement().getThemeList().add("success primary");
        loginButton.addClickListener(event -> loginUser());

        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.add(registerButton, loginButton);

        return buttonsLayout;
    }

    private Component buildLoginInformation() {
        VerticalLayout loginInformation = new VerticalLayout();
        loginInformation.setClassName("login-information");
        H1 loginInfoHeader = new H1("Login page");
        Span span = new Span("Try to login or register");
        span.setWidth("300px");

        loginInformation.add(loginInfoHeader, span);

        return loginInformation;
    }

    private void loginUser() {
        UserLoginData userLogin = new UserLoginData();
        if (userLoginBinder.writeBeanIfValid(userLogin)) {
            log.info("Trying to login user with credentials {}", userLogin);

            Optional<UserLoginResponse> userLoginResponse = userLoginHandler.loginUser(userLogin);
            if (userLoginResponse.isPresent()) {
                tokenService.saveUserToken(userLoginResponse.get().getToken());
                userLoginBinder.setBean(new UserLoginData());

                log.info("User {} has been logged in, navigating to Ok menu", userLoginResponse.get());
                new LoggedInDialog().open();
            } else {
                Notification.show("Invalid credentials, please check user/password combination!", (int) Duration.ofSeconds(3).toMillis(), Notification.Position.MIDDLE);
            }
        }
        userLoginBinder.validate();
    }
}
