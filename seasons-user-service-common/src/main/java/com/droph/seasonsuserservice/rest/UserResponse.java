package com.droph.seasonsuserservice.rest;

import lombok.Builder;
import lombok.Value;
import java.time.Instant;

@Value
@Builder(toBuilder = true)
public class UserResponse {
    Long id;
    String name;
    String email;
    Instant registerDate;
    Instant lastLoginDate;
    String token;
}
