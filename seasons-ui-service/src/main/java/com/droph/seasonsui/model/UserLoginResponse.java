package com.droph.seasonsui.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class UserLoginResponse {
    Long id;
    String token;
    String name;
}
