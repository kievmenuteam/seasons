package com.droph.seasonsui.ui.view;

import com.droph.seasonsui.service.TokenService;
import com.droph.seasonsui.service.UserGameSessionHandler;
import com.droph.seasonsui.service.UserLoginHandler;
import com.droph.seasonsui.ui.component.LogoutComponent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import lombok.extern.slf4j.Slf4j;

@Route("game")
@PageTitle("Game")
@Slf4j
public class GameView extends HorizontalLayout {
    private final TokenService           tokenService;
    private final UserLoginHandler       userLoginHandler;
    private final UserGameSessionHandler userGameSessionHandler;

    public GameView(TokenService tokenService,
                    UserLoginHandler userLoginHandler, UserGameSessionHandler userGameSessionHandler) {
        this.tokenService = tokenService;
        this.userLoginHandler = userLoginHandler;
        this.userGameSessionHandler = userGameSessionHandler;

        buildUI();
    }

    private void buildUI() {
        VerticalLayout verticalLayout  = new VerticalLayout();
        Component      logoutComponent = new LogoutComponent(tokenService, userLoginHandler, "Hello fellow gamer, game view under construction");


        verticalLayout.add(logoutComponent);

        verticalLayout.add(new Html("<br/>"));

        Button menuButton = new Button("Back to menu");
        menuButton.getElement().getStyle().set("width", "400px");
        menuButton.getElement().getStyle().set("height", "40px");
        menuButton.setIcon(VaadinIcon.ARROW_LEFT.create());
        menuButton.addClickListener(event -> {
            tokenService.getUserToken().ifPresent(userGameSessionHandler::endGame);
            UI.getCurrent().navigate(MenuView.class);
        });

        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.add(menuButton);

        verticalLayout.add(buttonsLayout);

        add(verticalLayout);
    }
}
