package com.droph.seasonsui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class SeasonsUiApplication {
    public static void main(String[] args) {
        SpringApplication.run(SeasonsUiApplication.class, args);
    }
}



