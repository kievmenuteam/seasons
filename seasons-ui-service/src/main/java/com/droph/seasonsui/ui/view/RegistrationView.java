package com.droph.seasonsui.ui.view;

import com.droph.seasonsui.model.UserRegistrationData;
import com.droph.seasonsui.service.UserRegistrationHandler;
import com.droph.seasonsuserserviceclient.client.exception.PasswordPolicyException;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.GeneratedVaadinTextField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.Binder.Binding;
import com.vaadin.flow.data.binder.Setter;
import com.vaadin.flow.data.binder.Validator;
import com.vaadin.flow.data.validator.EmailValidator;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.router.Route;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;

import static com.vaadin.flow.data.binder.ValidationResult.ok;

@Route("registration")
@Slf4j
public class RegistrationView extends VerticalLayout {
    private final UserRegistrationHandler userRegistrationHandler;
    private Binder<UserRegistrationData> userRegistrationBinder = new Binder<>();
    private TextField userNameField;

    public RegistrationView(UserRegistrationHandler userRegistrationHandler) {
        this.userRegistrationHandler = userRegistrationHandler;
        buildUI();

        userNameField.focus();
    }

    private void buildUI() {
        setSizeFull();

        FormLayout loginForm = buildLoginForm();

        add(loginForm);
    }

    private FormLayout buildLoginForm() {
        FormLayout registerFormLayout = new FormLayout();

        userNameField = new TextField();
        configureBinder("User name ", userNameField, UserRegistrationData::getName, UserRegistrationData::setName);

        PasswordField passwordField = new PasswordField();
        configureBinder("Password", passwordField, UserRegistrationData::getPassword, UserRegistrationData::setPassword);

        TextField emailField = new TextField();
        EmailValidator emailValidator = new EmailValidator("Please use valid email");
        configureBinder("User name ", emailField, UserRegistrationData::getEmail, UserRegistrationData::setEmail, emailValidator);


        registerFormLayout.addFormItem(userNameField, "Username");
        registerFormLayout.add(new Html("<br/>"));
        registerFormLayout.addFormItem(emailField, "Email");
        registerFormLayout.add(new Html("<br/>"));
        registerFormLayout.addFormItem(passwordField, "Password");
        registerFormLayout.add(new Html("<br/>"));

        registerFormLayout.add(buildButtonsLayout());

        registerFormLayout.getElement()
                          .addEventListener("keypress", event -> registerUser())
                          .setFilter("event.key == 'Enter'");

        return registerFormLayout;
    }

    private HorizontalLayout buildButtonsLayout() {
        Button backToLoginButton = new Button("Back to login page");
        backToLoginButton.getElement().getStyle().set("width", "200px");
        backToLoginButton.addClickListener(buttonClickEvent -> backToLoginButton.getUI()
                                                                                .ifPresent(ui -> ui.navigate(LoginView.class)));

        Button registerUserButton = new Button("Register account");
        registerUserButton.getElement().getStyle().set("width", "200px");
        registerUserButton.getElement().getThemeList().add("success primary");
        registerUserButton.addClickListener(event -> registerUser());

        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.add(backToLoginButton, registerUserButton);

        return buttonsLayout;
    }

    private <R extends GeneratedVaadinTextField<R, String>> void configureBinder(String fieldName,
                                                                                 GeneratedVaadinTextField<R, String> field,
                                                                                 ValueProvider<UserRegistrationData, String> getter,
                                                                                 Setter<UserRegistrationData, String> setter) {
        configureBinder(fieldName, field, getter, setter, (f, r) -> ok());
    }

    private <R extends GeneratedVaadinTextField<R, String>> void configureBinder(String fieldName,
                                                                                 GeneratedVaadinTextField<R, String> field,
                                                                                 ValueProvider<UserRegistrationData, String> getter,
                                                                                 Setter<UserRegistrationData, String> setter,
                                                                                 Validator<String> additionalValidator) {
        Binding<UserRegistrationData, String> binder = userRegistrationBinder.forField(field)
                                                                             .withValidator(pass -> !field.getValue()
                                                                                                          .trim()
                                                                                                          .isEmpty(), fieldName + " can not be empty")
                                                                             .withValidator(additionalValidator)
                                                                             .bind(getter, setter);
        field.addValueChangeListener(event -> binder.validate());
        field.setRequiredIndicatorVisible(true);
    }

    private void registerUser() {
        UserRegistrationData userRegistrationData = new UserRegistrationData();
        if (userRegistrationBinder.writeBeanIfValid(userRegistrationData)) {
            log.info("Trying to register user with credentials {}", userRegistrationData);
            try {
                userRegistrationHandler.registerUser(userRegistrationData);
                log.info("User successfully registered with credentials {}", userRegistrationData);
                new LoggedInDialog().open();
            } catch (PasswordPolicyException e) {
                log.error("Error while registering user {}", userRegistrationData, e);
                Notification.show("Password should contain upper and lowercase letters, at least one digit and at least one of these: !@#_%$?"
                        , (int) Duration.ofSeconds(3).toMillis(), Notification.Position.MIDDLE);
            }
        }
        userRegistrationBinder.validate();
    }
}
