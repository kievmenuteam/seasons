package com.droph.seasonsuserservice.configuration;

import com.droph.seasonsuserservice.common.AccountType;
import com.droph.seasonsuserservice.converters.UserEntityUserResponseConverter;
import com.droph.seasonsuserservice.converters.UserRequestUserEntityConverter;
import com.droph.seasonsuserservice.persistance.UserRepository;
import com.droph.seasonsuserservice.service.DefaultUserService;
import com.droph.seasonsuserservice.service.FbUserService;
import com.droph.seasonsuserservice.service.UserServiceFacade;
import com.droph.seasonsuserservice.service.login.UserLoginManager;
import com.droph.seasonsuserservice.service.login.UserLoginService;
import com.droph.seasonsuserservice.service.login.UserLoginServiceFacade;
import com.droph.seasonsuserservice.service.validation.UserValidator;
import com.droph.seasonsuserservice.service.validation.UserValidatorFacade;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import java.time.Clock;
import java.util.HashMap;
import java.util.Map;

import static com.droph.seasonsuserservice.common.AccountType.DEFAULT;
import static com.droph.seasonsuserservice.common.AccountType.FACEBOOK;

@Configuration

public class UserServiceConfiguration {

    @Bean
    public UserLoginManager userLoginManager() {
        return new UserLoginManager();
    }

    @Bean
    @DependsOn("userRepository")
    public UserServiceFacade userServiceFacade(UserRepository userRepository,
                                               UserLoginManager userLoginManager,
                                               UserEntityUserResponseConverter userEntityUserResponseConverter,
                                               FbUserService fbUserService,
                                               DefaultUserService defaultUserService) {
        Map<AccountType, UserLoginService> userServices = new HashMap<>();
        userServices.put(FACEBOOK, fbUserService);
        userServices.put(DEFAULT, defaultUserService);

        return new UserServiceFacade(userRepository, userLoginManager, userEntityUserResponseConverter, userServices);
    }

    @Bean
    public UserEntityUserResponseConverter userEntityUserResponseConverter() {
        return new UserEntityUserResponseConverter();
    }

    @Bean
    public UserRequestUserEntityConverter userRequestUserEntityConverter() {
        return new UserRequestUserEntityConverter();
    }

    @Bean
    public FbUserService fbUserService() {
        return new FbUserService();
    }

    @Bean
    public DefaultUserService defaultUserService(UserRepository userRepository,
                                                 UserLoginManager userLoginManager,
                                                 UserEntityUserResponseConverter userEntityUserResponseConverter,
                                                 UserRequestUserEntityConverter userRequestUserEntityConverter) {
        return new DefaultUserService(userRepository,
                                      userLoginManager,
                                      userEntityUserResponseConverter,
                                      userRequestUserEntityConverter,
                                      Clock.systemUTC());
    }

    @Bean
    public UserValidatorFacade userValidatorFacade(FbUserService fbUserService,
                                                   DefaultUserService defaultUserService) {
        Map<AccountType, UserValidator> userServices = new HashMap<>();
        userServices.put(FACEBOOK, fbUserService);
        userServices.put(DEFAULT, defaultUserService);

        return new UserValidatorFacade(userServices);
    }

    @Bean
    public UserLoginServiceFacade userLoginServiceFacade(FbUserService fbUserService,
                                                         DefaultUserService defaultUserService) {
        Map<AccountType, UserLoginService> userServices = new HashMap<>();
        userServices.put(FACEBOOK, fbUserService);
        userServices.put(DEFAULT, defaultUserService);

        return new UserLoginServiceFacade(userServices, defaultUserService);
    }
}
