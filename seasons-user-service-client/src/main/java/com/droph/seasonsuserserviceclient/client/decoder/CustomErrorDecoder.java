package com.droph.seasonsuserserviceclient.client.decoder;

import com.droph.seasonsuserserviceclient.client.exception.InvalidCredentialException;
import com.droph.seasonsuserserviceclient.client.exception.PasswordPolicyException;
import feign.Response;
import feign.codec.ErrorDecoder;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class CustomErrorDecoder implements ErrorDecoder {
    private final Map<Integer, Supplier<Exception>> decodeStrategy = initStrategies();
    private ErrorDecoder defaultDecoder = new Default();

    private Map<Integer, Supplier<Exception>> initStrategies() {
        Map<Integer, Supplier<Exception>> strategies = new HashMap<>();

        strategies.put(401, InvalidCredentialException::new);
        strategies.put(422, PasswordPolicyException::new);

        return strategies;
    }

    @Override
    public Exception decode(String s, Response response) {
        return decodeStrategy.getOrDefault(response.status(),
                                           () -> defaultDecoder.decode(s, response))
                             .get();
    }
}
