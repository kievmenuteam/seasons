package com.droph.seasonsuserservice.service.login;

import com.droph.seasonsuserservice.rest.UserRequest;
import com.droph.seasonsuserservice.rest.UserResponse;

import java.util.Optional;

public interface UserLoginService {
   UserResponse login(String user, String password);

   void addUser(UserRequest user);

   Optional<UserResponse> reLogin(String token);

}
