package com.droph.seasonsuserservice.service.login;

import com.droph.seasonsuserservice.common.AccountType;
import com.droph.seasonsuserservice.rest.UserRequest;
import com.droph.seasonsuserservice.rest.UserResponse;
import com.droph.seasonsuserservice.service.DefaultUserService;
import lombok.AllArgsConstructor;

import java.util.Map;
import java.util.Optional;

@AllArgsConstructor
public class UserLoginServiceFacade {
    private final Map<AccountType, UserLoginService> userServices;
    private final DefaultUserService defaultUserService;

    public UserResponse login(String user, String password, AccountType accountType) {
        UserLoginService userService = userServices.get(accountType);
        return userService.login(user, password);
    }

    public Optional<UserResponse> reLogin(String token) {
        return defaultUserService.reLogin(token);
    }

    public void addUser(UserRequest user) {
        UserLoginService userService = userServices.get(user.getAccountType());
        userService.addUser(user);
    }
}
