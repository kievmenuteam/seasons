package com.droph.seasonsuserservice.service;

import com.droph.seasonsuserservice.common.AccountType;
import com.droph.seasonsuserservice.converters.UserEntityUserResponseConverter;
import com.droph.seasonsuserservice.converters.UserRequestUserEntityConverter;
import com.droph.seasonsuserservice.exception.InvalidCredentialException;
import com.droph.seasonsuserservice.exception.PasswordPolicyException;
import com.droph.seasonsuserservice.persistance.UserEntity;
import com.droph.seasonsuserservice.persistance.UserRepository;
import com.droph.seasonsuserservice.rest.UserRequest;
import com.droph.seasonsuserservice.rest.UserResponse;
import com.droph.seasonsuserservice.service.login.UserLoginManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DefaultUserServiceTest {
    public static final Instant NOW = Instant.now();
    @Mock
    private UserRepository userRepository;
    @Mock
    private UserLoginManager userLoginManager;

    private UserRequestUserEntityConverter userRequestUserEntityConverter = new UserRequestUserEntityConverter();
    private UserEntityUserResponseConverter userEntityUserResponseConverter = new UserEntityUserResponseConverter();

    private DefaultUserService defaultUserService;

    @Before
    public void setUp() {
        defaultUserService = new DefaultUserService(userRepository,
                                                    userLoginManager,
                                                    new UserEntityUserResponseConverter(),
                                                    new UserRequestUserEntityConverter(),
                                                    Clock.fixed(NOW, ZoneId.systemDefault()));
    }

    @Test
    public void shouldPassValidate() {
        UserRequest user = UserRequest.builder()
                                      .name("Vova")
                                      .email("3@3.com")
                                      .password("Ololo2!")
                                      .accountType(AccountType.DEFAULT)
                                      .build();

        assertThat(defaultUserService.validate(user))
                .isTrue();
    }

    @Test
    public void shouldFailValidate() {
        UserRequest user = UserRequest.builder()
                                      .name("Vova")
                                      .email("3@3.com")
                                      .password("Ololo!")
                                      .accountType(AccountType.DEFAULT)
                                      .build();

        assertThatThrownBy(() -> defaultUserService.validate(user))
                .isExactlyInstanceOf(PasswordPolicyException.class)
                .hasMessageContaining("Password should contain upper and lowercase letters");
    }

    @Test
    public void shouldFailLoginNotRegisteredUser() {
        UserRequest user = UserRequest.builder()
                                      .name("Vova")
                                      .email("3@3.com")
                                      .password("Ololo2!")
                                      .accountType(AccountType.DEFAULT)
                                      .build();

        assertThatThrownBy(() -> defaultUserService.login(user.getEmail(), user.getPassword()))
                .isExactlyInstanceOf(InvalidCredentialException.class)
                .hasMessageContaining("Login failed!");

        Mockito.verify(userRepository).findByEmailAndPassword(user.getEmail(), user.getPassword());
    }

    @Test
    public void shouldNotLoginUserIfAlreadyLoggedIn() {
        String tokenMock = "true";
        UserRequest user = UserRequest.builder()
                                      .name("Vova")
                                      .email("3@3.com")
                                      .password("Ololo2!")
                                      .accountType(AccountType.DEFAULT)
                                      .build();

        UserEntity userEntity = getUserEntityFromRequest(user);
        UserResponse expectedResponse = userEntityUserResponseConverter.convert(userEntity)
                                                                       .toBuilder()
                                                                       .token(tokenMock)
                                                                       .build();

        when(userRepository.findByEmailAndPassword(user.getEmail(), user.getPassword()))
                .thenReturn(userEntity);

        when(userLoginManager.getUserToken(userEntity.getId()))
                .thenReturn(tokenMock);

        UserResponse actualResponse = defaultUserService.login(user.getEmail(), user.getPassword());

        assertThat(actualResponse)
                .isEqualTo(expectedResponse);
    }

    @Test
    public void shouldLoginUser() {
        UserRequest user = UserRequest.builder()
                                      .name("Vova")
                                      .email("3@3.com")
                                      .password("Ololo2!")
                                      .accountType(AccountType.DEFAULT)
                                      .build();

        UserEntity userEntity = getUserEntityFromRequest(user);
        UserResponse expectedResponse = userEntityUserResponseConverter.convert(userEntity);

        when(userRepository.findByEmailAndPassword(user.getEmail(), user.getPassword()))
                .thenReturn(userEntity);

        when(userLoginManager.getUserToken(userEntity.getId()))
                .thenReturn(null);

        when(userRepository.save(any(UserEntity.class)))
                .thenAnswer(returnsFirstArg());

        UserResponse actualResponse = defaultUserService.login(user.getEmail(), user.getPassword());
        verify(userLoginManager).logInUser(userEntity);
        assertThat(actualResponse)
                .isEqualTo(expectedResponse);
    }

    @Test
    public void shouldReloginUser() {
        String TOKEN = "token";
        UserRequest user = UserRequest.builder()
                                      .name("Vova")
                                      .email("3@3.com")
                                      .password("Ololo2!")
                                      .accountType(AccountType.DEFAULT)
                                      .build();

        UserEntity userEntity = getUserEntityFromRequest(user);
        UserResponse expectedResponse = userEntityUserResponseConverter.convert(userEntity)
                                                                       .toBuilder()
                                                                       .token(TOKEN)
                                                                       .build();

        when(userLoginManager.getUserByToken(TOKEN))
                .thenReturn(Optional.of(userEntity));

        Optional<UserResponse> reloginResponse = defaultUserService.reLogin(TOKEN);

        assertThat(reloginResponse).isNotEmpty().contains(expectedResponse);
    }

    @Test
    public void shouldReturnEmptyWhenReloginUser() {
        String TOKEN = "token";
        UserRequest user = UserRequest.builder()
                                      .name("Vova")
                                      .email("3@3.com")
                                      .password("Ololo2!")
                                      .accountType(AccountType.DEFAULT)
                                      .build();

        UserEntity userEntity = getUserEntityFromRequest(user);
        UserResponse expectedResponse = userEntityUserResponseConverter.convert(userEntity)
                                                                       .toBuilder()
                                                                       .token(TOKEN)
                                                                       .build();

        when(userLoginManager.getUserByToken(TOKEN))
                .thenReturn(Optional.empty());

        Optional<UserResponse> reloginResponse = defaultUserService.reLogin(TOKEN);

        assertThat(reloginResponse).isEmpty();
    }

    private UserEntity getUserEntityFromRequest(UserRequest user) {
        return UserEntity.builder().accountType(user.getAccountType())
                         .password(user.getPassword())
                         .name(user.getName())
                         .email(user.getEmail())
                         .lastLoginTs(NOW)
                         .id(1L)
                         .registrationTs(NOW)
                         .build();
    }
}