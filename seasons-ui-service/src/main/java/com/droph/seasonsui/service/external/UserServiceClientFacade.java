package com.droph.seasonsui.service.external;

import com.droph.seasonsui.model.UserLoginData;
import com.droph.seasonsui.model.UserRegistrationData;
import com.droph.seasonsuserservice.rest.UserRequest;
import com.droph.seasonsuserservice.rest.UserResponse;
import com.droph.seasonsuserserviceclient.client.UserServiceClient;
import com.droph.seasonsuserserviceclient.client.exception.InvalidCredentialException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;

import static com.droph.seasonsuserservice.common.AccountType.DEFAULT;

@Slf4j
@RequiredArgsConstructor
public class UserServiceClientFacade {
    private final UserServiceClient userServiceClient;

    public Optional<UserResponse> loginUser(UserLoginData userLogin) {
        try {
            log.info("Calling login on user-service with userLogin {}", userLogin);

            Optional<UserResponse> userResponse = Optional.ofNullable(userServiceClient.loginUser(userLogin.getLogin(),
                                                                                                  userLogin.getPassword(),
                                                                                                  DEFAULT.name()));

            log.info("Response {} for login {} received from user-service", userLogin, userResponse);
            return userResponse;
        } catch (InvalidCredentialException e) {
            log.error("Invalid credentials for {}", userLogin);
            return Optional.empty();
        }
    }

    public Optional<UserResponse> loginUser(String token) {
        log.info("Calling login on user-service with token {}", token);

        Optional<UserResponse> userResponse = userServiceClient.reLoginUser(token);

        log.info("Response {} received from user-service for token {}", userResponse, token);
        return userResponse;
    }

    public void logoutUser(String token) {
        userServiceClient.logoutUser(token);
    }

    public void registerUser(UserRegistrationData userRegistrationData) {
        log.info("Calling register on user-service with userRegistrationData {}", userRegistrationData);

        UserRequest userRequest = buildUserRequest(userRegistrationData);
        userServiceClient.registerUser(userRequest);

        log.info("User {} has been successfully registered", userRegistrationData);
    }

    private UserRequest buildUserRequest(UserRegistrationData userRegistrationData) {
        return UserRequest.builder()
                          .name(userRegistrationData.getName())
                          .email(userRegistrationData.getEmail())
                          .password(userRegistrationData.getPassword())
                          .accountType(DEFAULT)
                          .build();
    }
}
