package com.droph.seasonsuserservice.service.login;

import com.droph.seasonsuserservice.persistance.UserEntity;

import java.util.*;

public class UserLoginManager {
    private Map<Long, String> tokenByUser = new HashMap<>();
    private Map<String, UserEntity> userByToken = new HashMap<>();

    public String logInUser(UserEntity userEntity) {
        String token = UUID.randomUUID().toString();
        tokenByUser.put(userEntity.getId(), token);
        userByToken.put(token, userEntity);
        return token;
    }

    public boolean logOutUser(String token) {
        UserEntity removedUser = userByToken.remove(token);
        if (removedUser != null){
            tokenByUser.remove(removedUser.getId());
            return true;
        }
        return false;
    }

    public String getUserToken(Long userId) {
        return tokenByUser.get(userId);
    }

    public Optional<UserEntity> getUserByToken(String token) {
        return Optional.ofNullable(userByToken.get(token));
    }

    public Set<UserEntity> getAllLoggedInUsers() {
        return new HashSet<>(userByToken.values());
    }
}
