package com.droph.seasonsuserservice.exception;

public class InvalidCredentialException extends RuntimeException {
    public InvalidCredentialException(String msg) {
        super(msg);
    }
}
