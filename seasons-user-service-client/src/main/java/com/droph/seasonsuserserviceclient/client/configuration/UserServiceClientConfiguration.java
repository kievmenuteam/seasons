package com.droph.seasonsuserserviceclient.client.configuration;

import com.droph.seasonsuserserviceclient.client.decoder.CustomErrorDecoder;
import com.droph.seasonsuserserviceclient.client.decoder.CustomOptionalDecoder;
import feign.codec.ErrorDecoder;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.ribbon.FeignRibbonClientAutoConfiguration;
import org.springframework.cloud.openfeign.support.ResponseEntityDecoder;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AutoConfigureAfter(FeignRibbonClientAutoConfiguration.class)
@EnableFeignClients(basePackages = "com.droph.seasonsuserserviceclient.client")
public class UserServiceClientConfiguration {
    @Bean
    public ErrorDecoder CustomErrorDecoder() {
        return new CustomErrorDecoder();
    }

    @Bean
    public CustomOptionalDecoder feignDecoder(ObjectFactory<HttpMessageConverters> messageConverters) {
        return new CustomOptionalDecoder(new ResponseEntityDecoder(new SpringDecoder(messageConverters)));
    }
}
