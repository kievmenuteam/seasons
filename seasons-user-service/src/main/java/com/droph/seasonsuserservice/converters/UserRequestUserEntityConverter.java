package com.droph.seasonsuserservice.converters;

import com.droph.seasonsuserservice.persistance.UserEntity;
import com.droph.seasonsuserservice.rest.UserRequest;
import org.springframework.core.convert.converter.Converter;

public class UserRequestUserEntityConverter implements Converter<UserRequest, UserEntity> {
    @Override
    public UserEntity convert(UserRequest userRequest) {
        return UserEntity.builder()
                         .email(userRequest.getEmail())
                         .name(userRequest.getName())
                         .accountType(userRequest.getAccountType())
                         .password(userRequest.getPassword())
                         .build();
    }
}
