package com.droph.seasonsuserservice.service.validation;

import com.droph.seasonsuserservice.common.AccountType;
import com.droph.seasonsuserservice.rest.UserRequest;
import lombok.AllArgsConstructor;

import java.util.Map;

@AllArgsConstructor
public class UserValidatorFacade implements UserValidator {
    private final Map<AccountType, UserValidator> userServices;

    public boolean validate(UserRequest user) {
        return userServices.get(user.getAccountType()).validate(user);
    }
}
