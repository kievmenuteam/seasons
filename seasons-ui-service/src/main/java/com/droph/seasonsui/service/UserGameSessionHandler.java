package com.droph.seasonsui.service;

import com.droph.seasonsui.model.UserLoginResponse;
import com.droph.seasonsui.service.external.GameServiceClientFacade;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UserGameSessionHandler {
    private final GameServiceClientFacade gameServiceClientFacade;

    public boolean isGameSessionActiveForUser(UserLoginResponse userLoginResponse) {
        return gameServiceClientFacade.isGameSessionActiveForUser(userLoginResponse.getToken());
    }

    public void startGame(String userToken) {
        gameServiceClientFacade.startGame(userToken);
    }

    public void endGame(String userToken) {
        gameServiceClientFacade.endGame(userToken);
    }

}
