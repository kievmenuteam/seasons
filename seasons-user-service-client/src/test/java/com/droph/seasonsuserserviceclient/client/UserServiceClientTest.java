package com.droph.seasonsuserserviceclient.client;

import com.droph.seasonsuserservice.common.AccountType;
import com.droph.seasonsuserservice.rest.UserRequest;
import com.droph.seasonsuserservice.rest.UserResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder.okForJson;
import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT, classes = UserServiceClientTest.TestConfiguration.class)
@AutoConfigureWireMock(port = 8888)
public class UserServiceClientTest {
    @Autowired
    UserServiceClient userServiceClient;
    @Autowired
    ObjectMapper objectMapper;

    @Test
    public void shouldSayHello() {
        stubFor(get(urlPathEqualTo("/user-service/hello"))
                        .willReturn(aResponse()
                                            .withStatus(HttpStatus.OK.value())
                                            .withBody("Hello, Friend!"))
               );
        assertThat(userServiceClient.sayHello())
                .isEqualTo("Hello, Friend!");
    }

    @Test
    public void shouldListAllUsers() {
        UserResponse userResponse1 = UserResponse.builder()
                                                 .email("zalupa@mail.ru")
                                                 .build();

        UserResponse userResponse2 = UserResponse.builder()
                                                 .email("zalupa@ukr.net")
                                                 .build();

        List<UserResponse> userList = Arrays.asList(userResponse1, userResponse2);

        stubFor(get(urlPathEqualTo("/user-service/list/all"))
                        .willReturn(okForJson(userList))
               );

        assertThat(userServiceClient.getAllUsers()).contains(userResponse1).contains(userResponse2);
    }

    @Test
    public void shouldListLoggedInUsers() {
        UserResponse userResponse1 = UserResponse.builder()
                                                 .email("zalupa@mail.ru")
                                                 .build();

        UserResponse userResponse2 = UserResponse.builder()
                                                 .email("zalupa@ukr.net")
                                                 .build();

        List<UserResponse> userList = Arrays.asList(userResponse1, userResponse2);

        stubFor(get(urlPathEqualTo("/user-service/list/login-users"))
                        .willReturn(okForJson(userList))
               );

        assertThat(userServiceClient.getLoggedInUsers())
                .containsExactly(userResponse1, userResponse2);
    }

    @Test
    public void shouldLoginUser() {
        UserResponse userResponse = UserResponse.builder().id(1L).email("v@v.v").build();
        String userEmail = "valera@gmail.com";
        String userPassword = "12345Aa!";
        stubFor(put(urlPathEqualTo("/user-service/login"))
                        .withQueryParam("email", equalTo(userEmail))
                        .withQueryParam("pass", equalTo(userPassword))
                        .willReturn(okForJson(userResponse)));

        UserResponse actualUserResponse = userServiceClient.loginUser(userEmail, userPassword, null);

        assertThat(actualUserResponse).isEqualTo(userResponse);
    }

    @Test
    public void shouldRegisterUser() throws JsonProcessingException {
        UserRequest userRequest = UserRequest.builder()
                                             .email("v@v.v")
                                             .name("v")
                                             .password("vvv")
                                             .accountType(AccountType.DEFAULT)
                                             .build();

        String userServiceRegisterUrl = "/user-service/register";

        stubFor(post(urlPathEqualTo(userServiceRegisterUrl))
                        .withRequestBody(equalToJson(objectMapper.writeValueAsString(userRequest)))
                        .willReturn(ok()));

        userServiceClient.registerUser(userRequest);

        verify(postRequestedFor(urlEqualTo(userServiceRegisterUrl))
                       .withRequestBody(equalToJson(objectMapper.writeValueAsString(userRequest))));
    }

    @Test
    public void shouldLogOutUser() {
        String userServiceLogoutUrl = "/user-service/logout";
        String token = "1L";
        stubFor(delete(urlPathEqualTo(userServiceLogoutUrl))
                        .withQueryParam("token", equalTo(token))
                        .willReturn(ok()));

        userServiceClient.logoutUser(token);

        String userServiceLogoutUrlWithParams = userServiceLogoutUrl + "?token=" + token;
        verify(deleteRequestedFor(urlEqualTo(userServiceLogoutUrlWithParams)));
    }

    @Test
    public void shouldReloginUser() {
        String TOKEN = "token";
        UserResponse userResponse = UserResponse.builder()
                                                .token(TOKEN)
                                                .build();

        String userServiceReloginUrl = "/user-service/reLogin";
        stubFor(put(urlPathEqualTo(userServiceReloginUrl))
                        .withQueryParam("token", equalTo(TOKEN))
                        .willReturn(okForJson(userResponse)));

        Optional<UserResponse> actualResponse = userServiceClient.reLoginUser(TOKEN);
        assertThat(actualResponse).isNotEmpty().contains(userResponse);
    }

    @Test
    public void shouldEmptyEmptyWhenReloginUser() {
        String TOKEN = "token";

        String userServiceReloginUrl = "/user-service/reLogin";
        stubFor(put(urlPathEqualTo(userServiceReloginUrl))
                        .withQueryParam("token", equalTo(TOKEN))
                        .willReturn(ok()));

        Optional<UserResponse> actualResponse = userServiceClient.reLoginUser(TOKEN);
        assertThat(actualResponse).isEmpty();
    }

    @EnableAutoConfiguration
    public static class TestConfiguration {
    }
}