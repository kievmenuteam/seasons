package com.droph.seasonsuserservice.service;

import com.droph.seasonsuserservice.rest.UserRequest;
import com.droph.seasonsuserservice.rest.UserResponse;
import com.droph.seasonsuserservice.service.login.UserLoginService;
import com.droph.seasonsuserservice.service.validation.UserValidator;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FbUserService implements UserLoginService, UserValidator {

    @Override
    public boolean validate(UserRequest userRequest) {
        return true;
    }

    @Override
    public UserResponse login(String user, String password) {
        return null;
    }

    @Override
    public void addUser(UserRequest user) {

    }

    @Override
    public Optional<UserResponse> reLogin(String token) {
        return Optional.empty();
    }
}
