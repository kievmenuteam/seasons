package com.droph.seasonsuserservice.service;

import com.droph.seasonsuserservice.common.AccountType;
import com.droph.seasonsuserservice.converters.UserEntityUserResponseConverter;
import com.droph.seasonsuserservice.persistance.UserEntity;
import com.droph.seasonsuserservice.persistance.UserRepository;
import com.droph.seasonsuserservice.rest.UserResponse;
import com.droph.seasonsuserservice.service.login.UserLoginManager;
import com.droph.seasonsuserservice.service.login.UserLoginService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toSet;

@Service
@AllArgsConstructor
public class UserServiceFacade {
    private final UserRepository                  userRepository;
    private final UserLoginManager                userLoginManager;
    private final UserEntityUserResponseConverter userEntityUserResponseConverter;

    Map<AccountType, UserLoginService> userServices;

    public List<UserResponse> listUsers() {
        return userRepository.findAll()
                             .stream()
                             .map(userEntityUserResponseConverter::convert)
                             .collect(Collectors.toList());
    }

    public boolean logout(String token) {
        return userLoginManager.logOutUser(token);
    }

    public List<UserResponse> listLoggedInUsers() {
        Set<Long> userIds = userLoginManager.getAllLoggedInUsers().stream()
                                            .map(UserEntity::getId)
                                            .collect(toSet());
        return userRepository.findAllById(userIds)
                             .stream()
                             .map(userEntityUserResponseConverter::convert)
                             .collect(Collectors.toList());
    }
}
