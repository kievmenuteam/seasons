package com.droph.seasonsuserservice.service.validation;

import com.droph.seasonsuserservice.rest.UserRequest;

public interface UserValidator {
    boolean validate(UserRequest userRequest);
}
