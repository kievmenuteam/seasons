package com.droph.seasonsui.ui.component;

import com.droph.seasonsui.model.UserLoginResponse;
import com.droph.seasonsui.service.TokenService;
import com.droph.seasonsui.service.UserLoginHandler;
import com.droph.seasonsui.ui.view.LoginView;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.server.VaadinService;

import java.time.Duration;
import java.util.Optional;

import static com.vaadin.flow.component.notification.Notification.Position.MIDDLE;

public class LogoutComponent extends VerticalLayout {
    private final TokenService     tokenService;
    private final UserLoginHandler userLoginHandler;

    public LogoutComponent(TokenService tokenService, UserLoginHandler userLoginHandler, String text) {
        this.tokenService = tokenService;
        this.userLoginHandler = userLoginHandler;

        buildUi(text);
    }

    private void buildUi(String text) {
        VerticalLayout verticalLayout = new VerticalLayout();
        Label          generalText    = new Label(text);
        verticalLayout.add(generalText);
        verticalLayout.add(VaadinIcon.STOPWATCH.create());

        Component component = buildButtonsLayout();
        verticalLayout.add(component);

        add(verticalLayout);
    }

    private Component buildButtonsLayout() {
        Button logoutButton = new Button("Logout from game");
        logoutButton.getElement().getStyle().set("width", "250px");
        logoutButton.setIcon(VaadinIcon.ARROWS_CROSS.create());
        logoutButton.addClickListener(buttonClickEvent -> logoutUser());

        Button sayMyNameButton = new Button("Say my name");
        sayMyNameButton.getElement().getStyle().set("width", "150px");
        sayMyNameButton.setIcon(VaadinIcon.CHECK_CIRCLE.create());
        sayMyNameButton.addClickListener(event -> sayUserName());

        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.add(logoutButton, sayMyNameButton);

        return buttonsLayout;
    }

    private void logoutUser() {
        getUserToken().ifPresent(userLoginHandler::logoutUser);
        UI.getCurrent().navigate(LoginView.class);
    }

    private void sayUserName() {
        String userName = getUserToken().flatMap(userLoginHandler::loginUser)
                                        .map(UserLoginResponse::getName)
                                        .orElse("I dont know your name, friend!");
        Notification.show("Greetings, " + userName, (int) Duration.ofSeconds(2).toMillis(), MIDDLE);
    }

    private Optional<String> getUserToken() {
        return tokenService.getUserToken();
    }
}
