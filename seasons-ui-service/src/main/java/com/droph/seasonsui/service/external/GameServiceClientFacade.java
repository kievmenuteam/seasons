package com.droph.seasonsui.service.external;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class GameServiceClientFacade {
    private Map<String, String> playingUsers = new ConcurrentHashMap<>();

    public void startGame(String userToken) {
        playingUsers.put(userToken, userToken);
    }

    public void endGame(String userToken) {
        playingUsers.remove(userToken);
    }

    public boolean isGameSessionActiveForUser(String userToken) {
        return playingUsers.containsKey(userToken);
    }
}
