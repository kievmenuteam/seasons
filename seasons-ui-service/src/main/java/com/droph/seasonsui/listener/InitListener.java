package com.droph.seasonsui.listener;

import com.droph.seasonsui.model.UserLoginResponse;
import com.droph.seasonsui.service.TokenService;
import com.droph.seasonsui.service.UserGameSessionHandler;
import com.droph.seasonsui.service.UserLoginHandler;
import com.droph.seasonsui.ui.view.*;
import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.VaadinServiceInitListener;
import com.vaadin.flow.spring.annotation.SpringComponent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@Slf4j
@SpringComponent
public class InitListener implements VaadinServiceInitListener {
    @Autowired
    private TokenService           tokenService;
    @Autowired
    private UserLoginHandler       userLoginHandler;
    @Autowired
    private UserGameSessionHandler userGameSessionHandler;

    @Override
    public void serviceInit(ServiceInitEvent initEvent) {
        initEvent.getSource().addUIInitListener(uiInitEvent -> uiInitEvent.getUI().addBeforeEnterListener(enterEvent -> {
            if (isNavigationTargetShouldBeChecked(enterEvent.getNavigationTarget())) {
                log.info("Navigation target {} checking rules", enterEvent.getNavigationTarget());
                Optional<UserLoginResponse> userLoggedInWithToken = getUserToken().flatMap(userLoginHandler::loginUser);
                if (!userLoggedInWithToken.isPresent() && !LoginView.class.equals(enterEvent.getNavigationTarget())) {
                    log.info("User does not have active token, redirecting to login page");
                    enterEvent.rerouteTo(LoginView.class);
                } else if (userLoggedInWithToken.isPresent()) {
                    boolean isGameSessionActiveForUser = userLoggedInWithToken.filter(userGameSessionHandler::isGameSessionActiveForUser)
                                                                              .isPresent();
                    if (isGameSessionActiveForUser) {
                        if (!GameView.class.equals(enterEvent.getNavigationTarget())) {
                            log.info("User {} has active game session, redirecting to game", userLoggedInWithToken);
                            enterEvent.rerouteTo(GameView.class);
                        }
                    } else if (!MenuView.class.equals(enterEvent.getNavigationTarget())) {
                        log.info("User {} doesn't have active game session, redirecting to menu", userLoggedInWithToken);
                        enterEvent.rerouteTo(MenuView.class);
                    }
                }
            }
        }));
    }

    private boolean isNavigationTargetShouldBeChecked(Class<?> navigationTarget) {
        return !RegistrationView.class.equals(navigationTarget);
    }

    private Optional<String> getUserToken() {
        return tokenService.getUserToken();
    }

}

