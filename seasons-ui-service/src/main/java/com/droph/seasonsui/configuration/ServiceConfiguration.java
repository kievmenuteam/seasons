package com.droph.seasonsui.configuration;

import com.droph.seasonsui.service.TokenService;
import com.droph.seasonsui.service.UserGameSessionHandler;
import com.droph.seasonsui.service.UserLoginHandler;
import com.droph.seasonsui.service.UserRegistrationHandler;
import com.droph.seasonsui.service.external.GameServiceClientFacade;
import com.droph.seasonsui.service.external.UserServiceClientFacade;
import com.droph.seasonsuserserviceclient.client.UserServiceClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceConfiguration {
    @Bean
    public UserServiceClientFacade userServiceClientFacade(UserServiceClient userServiceClient) {
        return new UserServiceClientFacade(userServiceClient);
    }

    @Bean
    public UserLoginHandler userLoginHandler(UserServiceClientFacade userServiceClientFacade) {
        return new UserLoginHandler(userServiceClientFacade);
    }

    @Bean
    public TokenService tokenService() {
        return new TokenService();
    }

    @Bean
    public UserGameSessionHandler userGameSessionHandler() {
        return new UserGameSessionHandler(new GameServiceClientFacade());
    }

    @Bean
    public UserRegistrationHandler userRegistrationHandler(UserServiceClientFacade userServiceClientFacade,
                                                           UserLoginHandler userLoginHandler,
                                                           TokenService tokenService) {
        return new UserRegistrationHandler(userServiceClientFacade, userLoginHandler, tokenService);
    }
}
