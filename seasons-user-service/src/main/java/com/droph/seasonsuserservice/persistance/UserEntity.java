package com.droph.seasonsuserservice.persistance;

import com.droph.seasonsuserservice.common.AccountType;
import lombok.Builder;
import lombok.Value;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.Instant;


@Entity
@Value
@Builder(toBuilder = true)
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    String name;
    @Column(unique = true)
    String email;
    String password;
    AccountType accountType;
    @CreationTimestamp
    Instant registrationTs;
    Instant lastLoginTs;
}

