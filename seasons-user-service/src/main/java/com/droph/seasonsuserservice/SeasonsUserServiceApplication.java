package com.droph.seasonsuserservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class SeasonsUserServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(SeasonsUserServiceApplication.class, args);
    }
}
